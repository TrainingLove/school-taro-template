const wcl = require("./wcl");

const home = "pages/index/index"
const routes = [...wcl];
const result = [""];
for (let item of routes){
  if (item !== home)
    result.push(item);
  else result[0] = item;
}
module.exports = result;
