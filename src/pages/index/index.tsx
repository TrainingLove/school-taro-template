import { Component, PropsWithChildren } from 'react'
import { View, Text,Picker, PickerView, PickerViewColumn } from '@tarojs/components'
import {AtButton,AtInput,AtInputNumber,AtForm} from "taro-ui";
import Taro from '@tarojs/taro'
import './index.scss'
import {axios,loginAxios} from "../../request";

type IStaet = {
  value:any
}
export default class Index extends Component<PropsWithChildren,IStaet> {
  constructor(props:PropsWithChildren) {
    super(props);
    this.state = {
      value:0
    }
  }
  componentWillMount () {

  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  onchange(e){
    console.log(e);
  }
  render () {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <AtButton type={"primary"}>这是一个按钮</AtButton>
        <AtInput value={this.state.value} name={'name'} onChange={this.onchange}></AtInput>
        <AtForm>
          <AtInput name='inputValue1' title='标准五个字' type='text' placeholder='标准五个字' value={this.state.inputValue1} onChange={this.onchange.bind(this, 'inputValue1')} />
        </AtForm>
        <AtInputNumber
          min={0}
          max={10}
          step={1}
          value={this.state.inputNumber1}
          onChange={this.onchange.bind(this, 'inputNumber1')}
        />
      </View>
    )
  }
}
