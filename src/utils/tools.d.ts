interface IComputeText{
    size: number // 字体大小
    text: string // 字符串
}

declare namespace _default{
    function error(paramName:string):void;
    function contrastTime(beginTime:string, endTime:string, nowTime:string):void;
    function GUID(format:string):void;
    function getStorage(key:string):any;
    function setStorage(key:string, value:any):void;
    function setSession(key:string, value:any):void;
    function getSession(key:string):any;
    function CheckImgExists(imgUrl:string, callback:Function):void;
    function count_down(nums:number,callback:Function):void;
    function checkInput(type:string, data:any):boolean;
    function checkInputs(options:any, callback:Function):void;
    function secretId(data:string|number,type:number):string;
    function checkVersionUpdate(currentVersion:string, serverVersion:string):boolean;
    function formatMoney(num:number|string):string;
    function throttle(fn:Function, after:boolean, delay:number):(e:any)=>any;
    function debounce(fn:Function|null, after:boolean, delay:number):(e:any)=>any;
    function GetBrowser():any;
    function DFS(data:any, callback:Function, options:any):void;
    function length(obj:any):number;
    function jsonToMap(obj:any):any;
    function formatNumber(num:number|string):string;
    function jsonToArray(obj:any):Array<any>;
    function objectNumberToggle(obj:any,type:number):any;
    function formatDate (date:Date,fmt:string):string;
    function colorHEXToDEC(color:string,opacity:number):string;
    function isEmpty(val:string[]|string,reg:"&&"|"||"):boolean;
    function disableMousewheel():void;
    function setWxTitle(text:string):void;
    function listenPromiseCatch(callback:Function):void;
    function scaleImage(url:string,width:number,height:number):Promise<File>;
    function dataURLtoBlob(base64:string):Blob;
}
export default _default;
