export type EnvConfigContent = {
  API_BASE_URL:string,
  OSS_URL:string
}
export type EnvConfig = {
  development:EnvConfigContent
  testEnv:EnvConfigContent
  production:EnvConfigContent
  pre:EnvConfigContent
}
export type EnvTypes = keyof EnvConfig;
const env:EnvTypes = process.env.NODE_ENV as EnvTypes;
const config:EnvConfig = {
  development:{
    API_BASE_URL: 'http://api.dev.school.com/dev_api2/api',
    OSS_URL:"http://172.16.14.192:9000/bf-static/1v1-teacher/img/"
  },
  testEnv:{
    API_BASE_URL: 'http://api.test.school.com/test_api2/api',
    OSS_URL:"http://172.16.14.192:9000/bf-static/1v1-teacher/img/"
  },
  production:{
    API_BASE_URL: 'http://218.89.142.195:8000/pro_api2/api',
    OSS_URL:"http://172.16.14.192:9000/bf-static/1v1-teacher/img/"
  },
  pre:{
    API_BASE_URL: 'http://api.pre.school.com/pre_api2/api',
    OSS_URL:"http://172.16.14.192:9000/bf-static/1v1-teacher/img/"
  }
}

export default config[env];
